'''
Ein einfaches Programm, das zeigt, wie leicht
ein Spammer eMail-Adressen generieren kann.

Einsatz auf eigene Gefahr! User haften für ihre Scripte!

@author: Christoph Jüngling <chris@juengling-edv.de>
'''

import os
from random import randint, choice
from time import perf_counter, strftime

from access import connectstring
import pyodbc


def create_emails_for_known_providers(providers, count=100):
    '''
    Generiere zufällige eMails für bekannte Provider

    :param providers: Liste der Provider, mit denen gearbeitet werden soll
    :param count: Anzahl zu generierender eMail-Adressen
    '''

    result = []

    for _ in range(0, count):
        name = create_random_name()
        email = name + '@' + choice(providers)
        result.append(email)

    return result


def create_emails_for_known_tlds(count=100):
    '''
    Generiere zufällige eMails für bekannte TLDs

    :param count: Anzahl zu generierender eMail-Adressen
    '''

    result = []

    # Liste der verwendeten Top-Level-Domains
    # Für eine vollständige Liste gemäß ICANN siehe
    # https://www.icann.org/resources/pages/tlds-2012-02-25-en
    tlds = ['com', 'net', 'org', 'de', 'ch', 'at']

    for _ in range(0, count):
        name = create_random_name(2)
        provider = create_random_name(3, 7)
        email = '{mailname}@{mailprovider}.{tld}'.format(
            mailname=name,
            mailprovider=provider,
            tld=choice(tlds)
        )
        result.append(email)

    return result


def create_emails_for_known_names(providers):
    '''
    Generiere eMails für bekannte Namen aus der Datenbank

    :param providers: Liste der Provider, mit denen gearbeitet werden soll
    '''

    result = []

    # TODO: Diese beiden Listen aus der Datenbank lesen
    db_vornamen = ['Hans', 'Maria', 'Susi']
    db_nachnamen = ['Müller', 'Meier', 'Schulze']

    for vorname in db_vornamen:
        for nachname in db_nachnamen:
            email = '{vor}.{nach}@{prov}'.format(
                vor=vorname, nach=nachname, prov=choice(providers))
            result.append((vorname, nachname, email))

    return result


def create_random_name(minlength=5, maxlength=10):
    '''
    Generiert einen zufälligen Namen mit einer Länge aus dem
    angegebenen Bereich (jeweils einschließlich der Grenzen).

    :param minlength: Minimale Länge des Namens
    :param maxlength: Maximale Länge des Namens
    '''

    characters = [chr(i) for i in range(ord('a'), ord('z') + 1)] \
        + [chr(i) for i in range(ord('0'), ord('9') + 1)] \
        + ['-', '_']

    # Bestimme die Länge zufällig innerhalb der vorgegebenen Grenzen
    length = randint(minlength, maxlength)

    # Bilde den Namen mit der gewünschten Länge
    # durch zufällige Auswahl der Zeichen aus der
    # vorgegebenen Zeichenmenge
    name = ''

    for _ in range(0, length):
        name += choice(characters)

    return name


def store(mail, vorname='', nachname=''):
    connect = pyodbc.connect(
        connectstring(os.path.join(os.getcwd(), 'Mailadressen.accdb')))

    sql = "insert into adressen (vorname, nachname, email, getestet) values('{vor}', '{nach}', '{email}', '{date}')".format(
        vor=vorname, nach=nachname, email=mail, date=strftime("%Y-%m-%d %H:%M:%S"))
    try:
        rs = connect.cursor()
        rs.execute(sql)
        rs.commit()
    except Exception as e:
        print(e)
    finally:
        connect.close()


def mailinator1(count=100):
    providers = [
        'gmx.de',
        'gmx.at',
        'web.de',
        't-online.de',
        'yahoo.com',
    ]

    i = 0
    for email in create_emails_for_known_providers(providers, count):
        addr = '{:8}: {}'.format(i, email)
        print(addr)
        store(mail=email)
        i += 1


def mailinator2(count=100):
    i = 0
    for email in create_emails_for_known_tlds(count):
        addr = '{:8}: {}'.format(i, email)
        print(addr)
        store(mail=email)
        i += 1


def mailinator3():
    providers = [
        'gmx.de',
        'gmx.at',
        'web.de',
        't-online.de',
        'yahoo.com',
    ]

    i = 0
    for vorname, nachname, email in create_emails_for_known_names(providers):
        addr = '{:8}: {}'.format(i, email)
        print(vorname, nachname, email)
        store(vorname=vorname, nachname=nachname, mail=email)
        i += 1

if __name__ == '__main__':
    start = perf_counter()

    mailinator1()
    mailinator2()
    mailinator3()

    ende = perf_counter()
    print('Dauer: {:1.3f} s'.format(ende - start))
