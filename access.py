'''
Zugriff auf eine Access-Datenbank

Ein Beispiel für den Vortrag beim Access-Stammtisch Stuttgart

@author: Christoph Jüngling <chris@juengling-edv.de>
'''

import os
import pyodbc


def show_emails():
    '''
    Liste die eMail-Adressen aus der Datenbank
    '''

    connect = pyodbc.connect(
        connectstring(os.path.join(os.getcwd(), 'Mailadressen.accdb')))

    sql = 'select vorname, nachname, email from adressen order by email'
    try:
        rs = connect.cursor()
        rs.execute(sql)
        row = rs.fetchone()
        while row:
            print('{} {} <{}>'.format(row[0], row[1], row[2]))
            row = rs.fetchone()

    except Exception as e:
        print(e)


def connectstring(filename):
    '''
    Connectstring für die übergebene Access-Datenbank erzeugen und zurückgeben

    :param filename: Pfad zur Access-Datenbank
    '''
    return 'DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=' + filename

if __name__ == '__main__':
    show_emails()
