'''
Created on 21.05.2016

@author: chris
'''
import io
from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch
from reportlab.platypus import SimpleDocTemplate, Paragraph

from folie import folie


def agenda():
    for content in [ich, wir, python]:
        show(content())

class ich(folie):
    def __init__(self):
        self.title = 'Ich'
        self.content = [{'name':  'Christoph Jüngling'},
                        {'alter': 56},
                        {'beruf': 'Softwareentwickler'}]

class wir(folie):
    def __init__(self):
        self.title = 'Wir'
        self.content = []

class python(folie):
    def __init__(self):
        self.title = 'Python'
        self.content = []

def show(what):
    if what.title:
        line = '-' * len(what.title)
        print('\n{}\n{}\n{}\n'.format(line, what.title, line))

    if what.content:
        for topic in what.content:
            # Detect max. length of keys
            length = 0
            for line in topic:
                x = len(line)
                if x > length: length = x

        for topic in what.content:
            for line in topic:
                print('{key}{spaces}: {value}'.format(key = line, spaces = ' ' * (length - len(line)), value = topic[line]))
    else:
        print('Hier könnte Ihre Werbung stehen!')

    print('\n' + '=' * 50 + '\n')

def createpdf():
    buf = io.BytesIO()

    # Setup the document with paper size and margins
    doc = SimpleDocTemplate(
        buf,
        rightMargin = inch / 2,
        leftMargin = inch / 2,
        topMargin = inch / 2,
        bottomMargin = inch / 2,
        pagesize = letter,
    )

    # Styling paragraphs
    styles = getSampleStyleSheet()

    # Write things on the document
    paragraphs = []
    paragraphs.append(
        Paragraph('This is a paragraph', styles['Normal']))
    paragraphs.append(
        Paragraph('This is another paragraph', styles['Normal']))
    doc.build(paragraphs)

    # Write the PDF to a file
    with open('test.pdf', 'w') as fd:
        fd.write(buf.getvalue())



if __name__ == '__main__':
    # agenda()
    createpdf()
