# Von 0 auf Python in 45 Minuten #

Dieser Code entstand zur Demonstration einiger Möglichkeiten von Python im Rahmen eines Vortrages am 11.6.2016 zum 200. Stuttgarter Access-Stammtisch. Er ist nicht dazu bestimmt, tatsächlich Mailadressen zu generieren und für Spam-Aktivitäten verwendet zu werden. Aus diesem Grunde enthält der Code auch keinen Teil zum Versand der Mails.

### Lizenz ###

Der Code und das zum Download angebotene PDF dürfen im Sinne der **Gnu GPL** gerne frei verwendet werden. Dies beinhaltet eine kostenlose Benutzung und Veränderung, wobei allerdings jede Veränderung wiederum unter der gleichen Lizenz (Gnu GPL) veröffentlicht werden muss.